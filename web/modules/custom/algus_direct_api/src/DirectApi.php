<?php

/**
 * @return
 * Contains \Drupal\algus_direct_api\DirectApi
 */
 
namespace Drupal\algus_direct_api;

use Drupal\Core\StreamWrapper\PublicStream;

/**
 * Provides route responses for the algus_direct_api module.
 */
class DirectApi {

    public $token;

  
   public function __construct() {

   }


 public function execV5($auth_key_file, $service, $method, $params, $clientLogin="") {

     $this->token = file_get_contents(PublicStream::basePath().'/ya_direct/'.$auth_key_file);

     // echo $this->token;

       $url = 'https://api.direct.yandex.com/json/v5/'.$service;

       $headers = [
           'Host: api.direct.yandex.com',
           'Authorization: Bearer '.$this->token,
           'Client-Login: '.$clientLogin,
           'Accept-Language: ru',
           'Content-Type: application/json; charset=utf-8',
           'returnMoneyInMicros: false'
       ];

       $request = ['method'=> $method,
                   'params'=>$params];


       $body = json_encode($request);

       $curlInit = curl_init();

       curl_setopt($curlInit, CURLOPT_URL, $url);
       curl_setopt($curlInit, CURLOPT_POST, 1);

       curl_setopt($curlInit, CURLOPT_POSTFIELDS, $body);

       curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($curlInit, CURLOPT_HTTPHEADER, $headers);

       $curlExec = curl_exec($curlInit);

       curl_close($curlInit);

       if($service == 'reports') {
           return $curlExec;
       }
       else {
           //$yandexDirectApiResult = json_decode($curlExec, true); //массив клиентов (логин, название клиента)
           return json_decode($curlExec, true); //массив клиентов (логин, название клиента);
       }

  }
   
  // public function execV4($auth_key_file, $master_token, $method, $params, $clientLogin, $isFinance = true) {
  
  public function execV4($auth_key_file, $method, $params, $isFinance = true, $master_token="", $clientLogin="") {
    //  $start = microtime(true);
       $this->token = file_get_contents(PublicStream::basePath().'/ya_direct/'.$auth_key_file);

       $operation_num = 128;
       $finance_token = hash("sha256", $master_token . $operation_num . $method . $clientLogin);

       $headers = [
           'POST /v4/json/HTTP/1.1',
           'Host: api.direct.yandex.ru',
           'Accept-Language: ru',
           'Content-Type: application/json; charset=utf-8',
       ];

       if($isFinance == true) {
           $request = ['method' => $method,
                       'finance_token' => $finance_token,
                       'operation_num' => $operation_num,
                       'token' => $this->token,
                       'param' => $params];
       }

       else{
           $request = ['method'=> $method,
                       'token' => $this->token,
                       'param'=>$params];
           }


       $body = json_encode($request);

     // print('Скрипт подготовки к инициализации в V4 был выполнен за ' . (microtime(true) - $start) . ' секунд<br>');

   //   $start = microtime(true);
       $curlInit = curl_init();

       curl_setopt($curlInit, CURLOPT_URL,'https://api.direct.yandex.ru/live/v4/json/');
       curl_setopt($curlInit, CURLOPT_POST, 1);

        curl_setopt($curlInit, CURLOPT_POSTFIELDS, $body);

       curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($curlInit, CURLOPT_HTTPHEADER, $headers);

       $curlExec = curl_exec($curlInit);

       curl_close($curlInit);
     // print('Скрипт инициализации в V4 был выполнен за ' . (microtime(true) - $start) . ' секунд<br>');
     // print('Скрипт V4 был выполнен за ' . (microtime(true) - $start) . ' секунд<br>');

       return(json_decode($curlExec, true));
   }
}