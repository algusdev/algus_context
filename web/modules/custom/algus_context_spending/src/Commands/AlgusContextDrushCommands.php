<?php

namespace Drupal\algus_context_spending\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\StreamWrapper\PublicStream;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class AlgusContextDrushCommands extends DrushCommands
{

    /**
     * Drush command that imports accounts spendings from Yandex Direct into Database.
     * @param string $date
     * @command ac_update_spending_custom_commands:ac_update_spending
     * @aliases drush-ac_update_spending ac_update_spending
     * @option month
     *   Uppercase the message.
     * @usage ac_update_spending_custom_commands:ac_update_spending date --month
     */

  public function ac_update_spending($date = 'YESTERDAY', $options = ['month' => FALSE]){

      \Drupal::logger('algus_context_spending')->info("Началось обновление информации по расходам за указанную дату");


      $auth_key_file = 'token.txt';

      $yandex_api_params = [
          'SelectionCriteria' => ['Archived' => 'NO'],
          'FieldNames' => ['Login']

      ];


      $yandex_api_service = \Drupal::service('yandex_direct_api');

     // $start = microtime(true);

      $yandex_accounts_logins = $yandex_api_service->execV5($auth_key_file, 'agencyclients', 'get', $yandex_api_params); //забор всех неархивных аккаунтов клиентов yandex direct


     // echo date('Y-m-d', strtotime($date. " - 29 day"));


      if(($date == 'YESTERDAY')&&($options['month'])){

          $date = date('Y-m-d', strtotime('yesterday'));

          $yesterday_report_params = [
                   'SelectionCriteria' => ['DateFrom' => date('Y-m-d', strtotime($date. " - 29 day")),
              //'SelectionCriteria' => ['DateFrom' => date('Y-m-d', strtotime($date. " - 1 day")),
                       'DateTo' => $date],
                   'FieldNames' => ['Date', 'Cost'],
                   'ReportName' => 'MyReportName',
                   'ReportType' => 'ACCOUNT_PERFORMANCE_REPORT',
                   'DateRangeType' => 'CUSTOM_DATE',
                   'Format' => 'TSV',
                   'IncludeVAT' => 'YES', //Получение отчета о расхордах с НДС и без комиссии
                   'IncludeDiscount' => 'NO'
               ];

      }

      if(($date != 'YESTERDAY')&&($options['month'])){

          $yesterday_report_params = [
              'SelectionCriteria' => ['DateFrom' => date('Y-m-d', strtotime($date. " - 29 day")),
             // 'SelectionCriteria' => ['DateFrom' => date('Y-m-d', strtotime($date. " - 1 day")),
                  'DateTo' => $date],
              'FieldNames' => ['Date', 'Cost'],
              'ReportName' => 'MyReportName',
              'ReportType' => 'ACCOUNT_PERFORMANCE_REPORT',
              'DateRangeType' => 'CUSTOM_DATE',
              'Format' => 'TSV',
              'IncludeVAT' => 'YES', //Получение отчета о расхордах с НДС и без комиссии
              'IncludeDiscount' => 'NO'
          ];

      }

      if(!$options['month']){
          if($date == 'YESTERDAY'){
              $date = date('Y-m-d', strtotime('yesterday'));
          }

          $yesterday_report_params = [
              'SelectionCriteria' => ['DateFrom' => $date,
                  'DateTo' => $date],
              //'FieldNames' => ['Date', 'Clicks', 'Cost'],
              'FieldNames' => ['Date', 'Cost'],
              'ReportName' => 'MyReportName',
              'ReportType' => 'ACCOUNT_PERFORMANCE_REPORT',
              'DateRangeType' => 'CUSTOM_DATE',
              'Format' => 'TSV',
              'IncludeVAT' => 'YES', //Получение отчета о расхордах с НДС и без комиссии
              'IncludeDiscount' => 'NO'
          ];

          //Очистка всех данных с введенной датой, если --month == FALSE
          $query = \Drupal::database()->delete('ac_accounts_spending');
          $query->condition('date', $date);
          $query->execute();
      }

      else{
          $query = \Drupal::database()->delete('ac_accounts_spending');
          $query->condition('date', date('Y-m-d', strtotime($date. " - 29 day")), '>=');
          $query->condition('date', $date, '<=');
          $query->execute();
      }


      //Очистка всех данных с введенной датой (датами)
//        $query = \Drupal::database()->delete('ac_accounts_spending');
//        $query->condition('date', $date);
//        $query->execute();


       foreach($yandex_accounts_logins['result']['Clients'] as $item) {
          // $report_result = $yandex_api_service->execV5($auth_key_file, 'reports', 'get', $yesterday_report_params, 'super-trops');//передаем каждый login и получаем отчет

           $report_result = $yandex_api_service->execV5($auth_key_file, 'reports', 'get', $yesterday_report_params, $item['Login']);

          // echo $report_result;

          // echo $item['Login'];

           if(!(str_contains($report_result, 'rows: 0'))) {
             //  echo "row !=0";

              // разбиение кол-ва кликов и стоимость одного клика на массив
              $findString = 'Cost';
              $stringPosition = strpos($report_result, $findString);
              //echo $stringPosition;

              $findString = 'Total';
              $stringPosition2 = strpos($report_result, $findString);
              // echo $stringPosition1;

              $numString = substr($report_result, 0, $stringPosition2 - 1); //удаляем все с total
              //echo $str;

              $numString = substr($numString, $stringPosition + 5); //удаляем все до cost, включительно
              //echo $str;

              // echo $numString;
              $regArr = preg_split('/\s/', $numString); //разбиваем строку по пробелам на массив

              $date_spend_count = count($regArr)/2; //колчиество пар дата-расход

              // print_r($regArr);
              // echo $date_spend_count;

               $d=0;
               $s=1;

               for($i=0; $i<=$date_spend_count-1; $i++) {

                   //проверка на существование этой даты в БД

//                   $query = \Drupal::database()->select('ac_accounts_spending', 'aas');
//                   $query->addField('aas','date');
//                   $query->condition('aas.date', $regArr[$d]);
//                   $result = $query->execute();

                  // if ($result->fetchAssoc()) {

                       $query = \Drupal::database()->insert('ac_accounts_spending');
                       //$query = \Drupal::database()->update('ac_accounts_spending');
                       $query->fields([
                           'login',
                           'advert_system',
                           'date',
                           'spending'
                       ]);

                       $query->values([
                           $item['Login'],
                           'Я',
                           $regArr[$d],
                           $regArr[$s]
                       ]);

                       $query->execute();

                       $d += 2;
                       $s += 2;
                  // }
//                   else{
//                        $query = \Drupal::database()->insert('ac_accounts_spending');
//                      // $query = \Drupal::database()->update('ac_accounts_spending');
//                       $query->fields([
//                           'login',
//                           'advert_system',
//                           'date',
//                           'spending'
//                       ]);
//
//                       $query->values([
//                           $item['Login'],
//                           'Я',
//                           $regArr[$d],
//                           $regArr[$s]
//                       ]);
//
//                       $query->execute();
//
//                       $d += 2;
//                       $s += 2;
//                   }
               }

           }

           else{
              // echo "row == 0";
               if($options['month']){
                  // echo "month == true";
                   for($i=29; $i<=0; $i--){
                      // date('Y-m-d', strtotime($date. " - ".$i." day"));


                       //проверка на существование этой даты в БД
//                       $query = \Drupal::database()->select('ac_accounts_spending', 'aas');
//                       $query->addField('aas','date');
//                       $query->condition('aas.date', date('Y-m-d', strtotime($date . " - " . $i . " day")));
//                       $result = $query->execute();
//
//                       if ($result->fetchAssoc()) {

                           $query = \Drupal::database()->insert('ac_accounts_spending');
                           //$query = \Drupal::database()->update('ac_accounts_spending');
                           $query->fields([
                               'login',
                               'advert_system',
                               'date',
                               'spending'
                           ]);

                           $query->values([
                               $item['Login'],
                               'Я',
                               date('Y-m-d', strtotime($date . " - " . $i . " day")),
                               0
                           ]);

                           $query->execute();

                       //}

//                       else{
//                           $query = \Drupal::database()->insert('ac_accounts_spending');
//                           $query->fields([
//                               'login',
//                               'advert_system',
//                               'date',
//                               'spending'
//                           ]);
//
//                           $query->values([
//                               $item['Login'],
//                               'Я',
//                               date('Y-m-d', strtotime($date . " - " . $i . " day")),
//                               0
//                           ]);
//
//                           $query->execute();
//                       }


                   }
               }

               else{
                   //echo "month == false";
                   //проверка на существование этой даты в БД
//                   $query = \Drupal::database()->select('ac_accounts_spending', 'aas');
//                   $query->addField('aas','date');
//                   $query->condition('aas.date', date('Y-m-d', strtotime($date . " - " . $i . " day")));
//                   $result = $query->execute();
//
//                   if ($result->fetchAssoc()) {
                       $query = \Drupal::database()->insert('ac_accounts_spending');
                       //$query = \Drupal::database()->update('ac_accounts_spending');
                       $query->fields([
                           'login',
                           'advert_system',
                           'date',
                           'spending'
                       ]);

                       $query->values([
                           $item['Login'],
                           'Я',
                           $date,
                           0
                       ]);

                       $query->execute();
                   //}
//                   else{
//                       $query = \Drupal::database()->insert('ac_accounts_spending');
//                       $query->fields([
//                           'login',
//                           'advert_system',
//                           'date',
//                           'spending'
//                       ]);
//
//                       $query->values([
//                           $item['Login'],
//                           'Я',
//                           $date,
//                           0
//                       ]);
//
//                       $query->execute();
//                   }
               }
           }

       }


      //print('Запрос на забор всех неархивных аккаунтов клиентов yandex direct' . (microtime(true) - $start) . ' секунд');

      \Drupal::logger('algus_context_spending')->info("Закончилось обновление информации по расходам за указанную дату");
  }

}