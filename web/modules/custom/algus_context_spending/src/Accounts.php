<?php

/**
 * @return
 * Contains \Drupal\algus_context_spending\Accounts
 */
 
namespace Drupal\algus_context_spending;

/**
 * Provides route responses for the algus_context_spending module.
 */
 
class Accounts {

  
 public function __construct() {

   }
   
 public function getPlanSpendings($spreadsheet_id, $sheet_name, $auth_key_file) {
       
       $google_api_service = \Drupal::service('algus_google_api.sheets');
       $google_service_sheets = $google_api_service -> initGoogleServiceSheets($auth_key_file);
       
       $spreadsheet_response = $google_service_sheets->spreadsheets->get($spreadsheet_id);
       $sheet = $spreadsheet_response->getSheets();
       
        // Свойства листа
       // $sheet_properties = $sheet[$sheet_id]->getProperties();
        $sheet_properties = $sheet[0]->getProperties();

        $grid_properties = $sheet_properties->getGridProperties();
        $row_count = $grid_properties->rowCount;

        $range = $sheet_name.'!A2:F'.$row_count;

        $range_response = $google_service_sheets->spreadsheets_values->get($spreadsheet_id, $range);
        $google_sheet_values = $range_response->getValues();  
        
        foreach($google_sheet_values as &$item) //запись в массив $accounts аккаунтов yandex и google
              {
                  $login = $item[2]; //забор логина аккаунта клиента
                  if($item[1] == 'Я')
                  {
                      $accounts['ynd'][$login]['client_name']=$item[0];                      
                      $accounts['ynd'][$login]['advert_system']=$item[1];
                      $accounts['ynd'][$login]['login']=$item[2];
                      $accounts['ynd'][$login]['monthly_budget']+=(int)$item[4]; //с НДС и с комиссией
                      $accounts['ynd'][$login]['fee']=$item[5];
                  }

                  elseif($item[1] == 'G')
                  {
                      $accounts['ggl'][$login]['client_name']=$item[0];                      
                      $accounts['ggl'][$login]['advert_system']=$item[1];
                      $accounts['ggl'][$login]['login']=$item[2];
                      $accounts['ggl'][$login]['monthly_budget']+=(int)$item[4]; //с НДС и с комиссией
                      $accounts['ggl'][$login]['fee']=$item[5];
                      
                  }

              }

       // print_r($accounts);

          return $accounts;
   }

    public function getMoneyTransfersValues($spreadsheet_id, $clients_sheet_name, $transfers_sheet_name, $auth_key_file) {
        $google_api_service = \Drupal::service('algus_google_api.sheets');
        $google_service_sheets = $google_api_service -> initGoogleServiceSheets($auth_key_file);

        $spreadsheet_response = $google_service_sheets->spreadsheets->get($spreadsheet_id); //получение таблицы по id
        $sheet = $spreadsheet_response->getSheets();

        // Свойства листа
        $sheet_properties = $sheet[4]->getProperties();

        $grid_properties = $sheet_properties->getGridProperties();
        $row_count = $grid_properties->rowCount;


        $range = $clients_sheet_name.'!A2:E'.$row_count;

        $range_response = $google_service_sheets->spreadsheets_values->get($spreadsheet_id, $range);
        $google_sheet_values = $range_response->getValues();

        foreach($google_sheet_values as &$item) //создание массива $clients с необходимыми данными из GS
        {
            $client = $item[0]; //забор логина аккаунта клиента
            $clients[$client]['login']=$item[4];
        }

       // print_r($clients);

        $sheet_properties = $sheet[0]->getProperties();

        $grid_properties = $sheet_properties->getGridProperties();
        $row_count = $grid_properties->rowCount;


        $range = $transfers_sheet_name.'!B2:G'.$row_count;

        $range_response = $google_service_sheets->spreadsheets_values->get($spreadsheet_id, $range);

//        unset($google_sheet_values);
        $google_sheet_values = $range_response->getValues();

        foreach($google_sheet_values as &$item) //создание массива $transfers с необходимыми данными из GS
        {
            $client = $item[1]; //забор логина аккаунта клиента
            $transfers[$client]['transfer_date']=$item[0];
            $transfers[$client]['money_transfer']=$item[5];
        }

       // print_r($transfers);

       // print_r($clients);

        foreach ($transfers as $key => &$item){ 
            $login = $clients[$key]['login']; //получили логин этого клиента
            $total_transfers[$login]['transfer_date'] = $transfers[$key]['transfer_date'];
            $total_transfers[$login]['money_transfer'] = $transfers[$key]['money_transfer'];
        }

       // print_r($total_transfers);

        return $total_transfers;

    }
 
    
 public function getAccountsBalances($auth_key_file){
     $accounts_balances = $this -> getYandexAccountsBalances($auth_key_file);

     //Здесь будет вызов getGoogleAccountsBalances()
     
     return $accounts_balances;
     
 } 

 public function getYandexAccountsBalances($auth_key_file){
   //  $start = microtime(true);
     $yandex_api_params = [
          'SelectionCriteria' => ['Archived' => 'NO'],
          'FieldNames' => ['Login']
      ];
      
     $yandex_api_service = \Drupal::service('yandex_direct_api');
     
     $yandex_accounts_logins = $yandex_api_service->execV5($auth_key_file, 'agencyclients', 'get', $yandex_api_params); //забор всех неархивных аккаунтов клиентов yandex direct

    // print_r($yandex_accounts_logins);
    // print('Скрипт был выполнен за ' . (microtime(true) - $start) . ' секунд');

    // $start = microtime(true);
     $clientsLogins = array_column($yandex_accounts_logins['result']['Clients'], 'Login'); //создание из двумерного массива в одномерный со списком логинов клиентский аккаунтов
    // print('Скрипт был выполнен за ' . (microtime(true) - $start) . ' секунд');
     //print_r($clientsLogins);


     //Необходимо получить общее число логинов клиентских аккаунтов.
     //Если их более 50, то массив логинов для отправки запроса на получение балансов необходимо разбить
     //Разбиение по 30 элементов

     if(count($clientsLogins) > 30) $chunked_array = array_chunk($clientsLogins, 30);

     //$start = microtime(true);
     foreach($chunked_array as $item){ //каждый подмассив массива логинов отправляем в запрос на получение балансов
          $account_budget_params = array(
              'Action' => 'Get',
              'SelectionCriteria' => array(
                  //'Logins' => $accounts_logins
                  'Logins' => $item
              )
          );

        // $start = microtime(true);
         //$start = microtime(true);
         $yandex_accounts_logins[] = $yandex_api_service->execV4($auth_key_file, 'AccountManagement', $account_budget_params, false); //забор балансов yandex direct
         //print('Скрипт получения балансов был выполнен за ' . (microtime(true) - $start) . ' секунд<br>');
        // print('Скрипт получения балансов был выполнен за ' . (microtime(true) - $start) . ' секунд<br>');

     }
     //print('Скрипт был выполнен за ' . (microtime(true) - $start) . ' секунд');

     //$start = microtime(true);

     foreach($yandex_accounts_logins as $item){
         if(is_array($item['data']['Accounts'])){
             foreach($item['data']['Accounts'] as $item2) {
                 $login = $item2['Login'];
                 $balances['ynd'][$login] = $item2['Amount']*1.2; //Балансы с НДС
             }
         }
         else continue;
     }

    // print('Скрипт был выполнен за ' . (microtime(true) - $start) . ' секунд');

    return $balances;
 }

  public function getGoogleAccountsBalances(){

    }


  public function getAccountsSpendings(){

      $yesterday = date('Y-m-d',strtotime('yesterday')); //Y-m-d вчера
      $last7_days = date('Y-m-d',strtotime("yesterday - 7 day")); //Y-m-d 7 дней назад
      $last30_days = date('Y-m-d',strtotime("yesterday - 30 day")); //Y-m-d 30 дней назад

//      echo $yesterday;
//      echo $last7_days;
//      echo $last30_days;


      $query = \Drupal::database()->select('ac_accounts_spending', 'as');

      //--------------- Выборка за вчера

      $query->fields('as', ['login','advert_system', 'spending']); //выборка нужных полей за вчера
      $query->condition('as.date', $yesterday);
      $output = $query->execute();


          while ($rows = $output->fetchAssoc()) { //добавление данных в массив $spendings за вчера
           if ($rows['advert_system'] == 'Я') {
               $count += 1;
               $spendings['ynd'][$rows['login']]['yesterday_spending'] = $rows['spending']; //с НДС без комиссии
           } else $spendings['ggl'][$rows['login']]['yesterday_spending'] = $rows['spending'];
       }


          //--------------- Выборка по дате за 7 дней

          $query = \Drupal::database()->select('ac_accounts_spending', 'as'); //выборка нужных полей за последние 7 дней

          $query = \Drupal::database()->select('ac_accounts_spending', 'as');
          $query->fields('as', ['login', 'advert_system']);
          $query->condition('as.date', $last7_days, '>');
          $query->condition('as.date', $yesterday, '<=');
          $query->addExpression('SUM(spending)', 'last7_spending');
          $query->groupBy('as.login');
          $query->groupBy('as.advert_system');
          $output = $query->execute();


          while ($rows = $output->fetchAssoc()) { //добавление данных в массив $spendings за последнии 7 дней
              if ($rows['advert_system'] == 'Я') {
                  $spendings['ynd'][$rows['login']]['last7_spending'] = round($rows['last7_spending']); //с НДС без комиссии
              } else $spendings['ggl'][$rows['login']]['last7_spending'] = round($rows['last7_spending']);
          }


          //--------------- Выборка по дате за 30 дней

      $query = \Drupal::database()->select('ac_accounts_spending', 'as');
      $query->fields('as', ['login', 'advert_system']);
      $query->condition('as.date', $last30_days, '>');
      $query->condition('as.date', $yesterday, '<=');
      $query->addExpression('SUM(spending)', 'last30_spending');
      $query->groupBy('as.login');
      $query->groupBy('as.advert_system');
      $output = $query->execute();


          while ($rows = $output->fetchAssoc()) { //добавление данных в массив $spendings за последнии 30 дней
              if ($rows['advert_system'] == 'Я') {
                  $spendings['ynd'][$rows['login']]['last30_spending'] = round($rows['last30_spending']); //с НДС
              } else $spendings['ggl'][$rows['login']]['last30_spending'] = round($rows['last30_spending']);
          }


          //---------------Перебор дат с шагом 1 день от даты вчера до даты "30 дней назад"
          $count = 0; //количество дней, которые надо отнять от даты "вчера"
          $lostDates = []; //массив для хранения дат, которых не хватает в таблице БД
          //while((date('Y-m-d',strtotime('yesterday-'.$count." day"))) >= $last30_days) //пока перебираемая дата >= дате "30 дней назад"
            while((date('Y-m-d',strtotime('yesterday-'.$count." day"))) > $last30_days) //пока перебираемая дата >= дате "30 дней назад"
          {
              //проверяем наличие этой даты в таблице ac_accounts_spending в БД
              $searchingDate = date('Y-m-d',strtotime('yesterday-'.$count." day"));

              $query = \Drupal::database()->select('ac_accounts_spending', 'as');

              $query->addField('as', 'id'); //выборка записей, где дата в поле date == $searchingDate
              $query->condition('as.date', $searchingDate);
              $output = $query->countQuery()->execute()->fetchField();

              if($output == 0){ //если искомой даты нет в таблице БД
                  $lostDates[] = $searchingDate;
              }

            $count++;

          }

          $lostDates = array_reverse($lostDates);

          $drupalMessage =implode('<br>', $lostDates); //строка, содержащая отсутствующие даты в таблице БД

        if($drupalMessage != "") \Drupal::messenger()->addMessage(t('Не хватает информации по датам:<br>'.$drupalMessage), 'warning');


          return $spendings;
      }



 //}
 
 public function getCellOpacity($devision){
     
      if($devision == 10){
         $devision_opacity = 30;
         return $devision_opacity;
         
     } 
     
     if($devision >= 60 || $devision < 10) {
         $devision_opacity = 100;
         return $devision_opacity;
     }
     
     return ($devision - 10) * 1.4 + 30;
     

 }
 
 public function composePlanSpendings($plan_spendings, $balances, $spendings, $money_transfers_values, $auth_key_file){

  foreach ($balances['ynd'] as  $key => $item){ //массив balances меняется , оступ не по ключу, в по $item (login)
      
      if(!isset($plan_spendings['ynd'][$key]['login']))
      {
          $plan_spendings['ynd'][$key]['login'] = $key;
      }

      if(!isset($plan_spendings['ynd'][$key]['advert_system']))
      {
          $plan_spendings['ynd'][$key]['advert_system'] = 'Я';
      }
      

       if(!isset($plan_spendings['ynd'][$key]['client_name'])) //если нет имени клиента в Google Sheets, то отправить запрос на получение имени клиента по его логину
      {

          $yandex_api_params = [
              'SelectionCriteria' => ['Archived' => 'NO', 'Logins' => [$plan_spendings['ynd'][$key]['login']]],
              'FieldNames' => ['ClientInfo']

          ];

          $yandex_api_service = \Drupal::service('yandex_direct_api');

          $yandex_accounts_logins = $yandex_api_service->execV5($auth_key_file, 'agencyclients', 'get', $yandex_api_params); //забор всех неархивных аккаунтов клиентов yandex direct

          $plan_spendings['ynd'][$key]['client_name'] = $yandex_accounts_logins['result']['Clients'][0]['ClientInfo'];
      }

      
      if(!isset($plan_spendings['ynd'][$key]['fee']))
      {
          $plan_spendings['ynd'][$key]['fee'] = '0%';
      }
      

    $plan_spendings['ynd'][$key]['balance'] = round($item); //с НДС

  }

  /* ------------ ДЛЯ GOOGLE АККАУНТОВ
    foreach ($balances['ggl'] as  $key => $item){
       if(!isset($plan_spendings['ggl'][$key]['login']))
      {
          $plan_spendings['ggl'][$key]['login'] = $key;
      }

      if(!isset($plan_spendings['ggl'][$key]['advert_system']))
      {
          $plan_spendings['ggl'][$key]['advert_system'] = 'G';
      }
      
      if(!isset($plan_spendings['ynd'][$key]['fee']))
      {
          $plan_spendings['ggl'][$key]['fee'] = '0%';
      }
      
         $plan_spendings['ggl'][$key]['balance'] = round($item);
     }

  */

   foreach($spendings['ynd'] as $key => $item){

       $plan_spendings['ynd'][$key]['yesterday_spending'] = $item['yesterday_spending']; //с НДС и без комиссии
       $plan_spendings['ynd'][$key]['last7_spending'] = $item['last7_spending']; //с НДС и без комиссии
       $plan_spendings['ynd'][$key]['last30_spending'] = $item['last30_spending']; //с НДС и без комиссии

   }

   //print_r($money_transfers);

   foreach($money_transfers_values as $key => $item){
     //  echo $key."---".$item['money_transfer']."---".$item['transfer_date']."<br>";
       if(isset($plan_spendings['ynd'][$key])){
           $plan_spendings['ynd'][$key]['money_transfer'] = $item['money_transfer'];
           $plan_spendings['ynd'][$key]['transfer_date'] = $item['transfer_date'];
       }
//       $plan_spendings['ynd'][$key]['money_transfer'] = $item['money_transfer'];
//       $plan_spendings['ynd'][$key]['transfer_date'] = $item['transfer_date'];
   }

    // print_r($plan_spendings);

   /* ------------ ДЛЯ GOOGLE АККАУНТОВ
      foreach($spendings['ggl'] as $key => $item){
         $plan_spendings['ggl'][$key]['yesterday_spending'] = $item['yesterday_spending'];
         $plan_spendings['ggl'][$key]['last7_spending'] = $item['last7_spending'];
         $plan_spendings['ggl'][$key]['last30_spending'] = $item['last30_spending'];

     }
   */

    // print_r($plan_spendings['ynd']);

   foreach($plan_spendings['ynd'] as &$item){
       
       if(!array_key_exists($item['login'], $balances['ynd'])) $item['balance'] = 'Нет такого аккаунта в Yandex Direct';

       $percent = (100 - (float)preg_replace('/%/','', $item['fee'])) * 0.01;
      // $percent = (100 - preg_replace('/%/','', $item['fee'])) * 0.01;


       //-----------------расходы с НДС
       $item['yesterday_spending'] = round($item['yesterday_spending']); //с НДС и без комиссии
       $item['last7_spending'] = round($item['last7_spending']); //с НДС и без комиссии
       $item['last30_spending'] = round($item['last30_spending']); //с НДС и без комиссии



       //------------------Бюджеты с НДС
       $item['monthly_budget'] =  round($item['monthly_budget']  * $percent); //с НДС и без комиссии
       $item['weekly_budget'] =  round($item['monthly_budget'] / 30 * 7); //с НДС и без комиссии
       $item['daily_budget'] = round($item['weekly_budget'] / 7); //с НДС и без комиссии

       if($item['last7_spending'] == 0) {
           $item['days_left'] = 0;
           $item['daily_devision'] = 0;
       }
       else {
          // $item['days_left'] = round((int)$item['balance'] / (int)$item['daily_budget'],1);

           $item['days_left'] = round((int)$item['balance'] / round((int)$item['last7_spending']/7),1);

           if($item['daily_budget'] == 0) $item['daily_devision'] = 0;
           else $item['daily_devision'] = -1 * round(($item['daily_budget'] - $item['yesterday_spending']) * 100 / $item['daily_budget'], 1);
       }


       if($item['daily_budget'] == $item['yesterday_spending']) $item['daily_devision'] = 0;


       if($item['weekly_budget'] == 0) $item['weekly_devision'] = 0;
       else $item['weekly_devision'] = -1 * round(($item['weekly_budget'] - $item['last7_spending']) * 100 / $item['weekly_budget'], 1);

       if($item['weekly_budget'] == $item['last7_spending']) $item['weekly_devision'] = 0;


       if($item['monthly_budget'] == 0)  $item['monthly_devision'] = 0;
       else $item['monthly_devision'] = -1 * round(($item['monthly_budget'] - $item['last30_spending']) * 100 / $item['monthly_budget'], 1);

       if($item['monthly_budget'] == $item['last30_spending']) $item['monthly_devision'] = 0;
       
       //Добавления дополнительного значения в масив $plan_spendings, обозначающего последующую прозрачность ячеек таблицы
       
        $item['monthly_devision_opacity'] = $this -> getCellOpacity(abs($item['monthly_devision']));
        $item['weekly_devision_opacity'] = $this -> getCellOpacity(abs($item['weekly_devision']));
        $item['daily_devision_opacity'] = $this -> getCellOpacity(abs($item['daily_devision']));

   }


    // print_r($plan_spendings);
     return $plan_spendings;

 }


}