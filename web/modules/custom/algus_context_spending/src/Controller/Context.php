<?php

/**
 * @return
 * Contains \Drupal\algus_context_spending\Controller\Context
 */
 
namespace Drupal\algus_context_spending\Controller;

use Drupal\Core\Controller\ControllerBase;

 
/**
 * Provides route responses for the algus_context_spending module.
 */
 
class Context extends ControllerBase {

  public function getSpendingsControl()
  {

      // $start = microtime(true);

      $yandex_auth_key_file = 'token.txt';
      $google_auth_key_file = 'google_api.json';


      //$account_service = \Drupal::service('algus_context_spending.accounts');

      $account_service = \Drupal::service('algus_context_spending.accounts');



          $plan_spendings = $account_service -> getPlanSpendings('1zBQxFL3JlMOuR5IqrMBdnDvQOkV4QZT2RI2QzuSbAy8', 'Кампании', $google_auth_key_file);
          //print_r($plan_spendings);

          $money_transfers_values = $account_service -> getMoneyTransfersValues('1ru_dZ6pN59T957ZiLzsBafz5kGBWR29-tXYSuT3W0co', 'Клиенты', 'ЗАЧИСЛЕНИЯ', $google_auth_key_file);

          $balances = $account_service -> getAccountsBalances($yandex_auth_key_file);
          // print_r($balances);


          $spendings = $account_service -> getAccountsSpendings();
          //print_r($spendings);

      

          $spending_report = $account_service -> composePlanSpendings($plan_spendings, $balances, $spendings, $money_transfers_values, $yandex_auth_key_file);
          //print_r($spending_report);

         // print('Скрипт Context был выполнен за ' . (microtime(true) - $start) . ' секунд');
          return [
              '#theme' => 'algus_context_spending',
              '#spendings_control' => $spending_report,
              '#cache' => ['max-age' => 0]
          ];


      }

  public function getDataLoadingControl(){


//      $my_menu = \Drupal::entityTypeManager()->getStorage('menu_link_content')->create([
//
//            'title' => 'ТЕСТ-МЕНЮ',
//            'link' => ['uri' => 'internal:/admin/structure'],
//            'menu_name' => 'account',
//            //'parent' => $top_level,
//            // 'expanded' => TRUE,
//            // 'weight' => 0,
//        ]);
//        $my_menu->save();



          $query = \Drupal::database()->select('ac_accounts_spending', 'as');


          $query->addField('as', 'date'); //выборка нужных полей за вчера
          $query->addExpression('COUNT(date)', 'count');
          $query->orderBy('as.date', 'DESC');
          $query -> range(0,50);
          $query->groupBy('as.date');

          //$query->condition('as.date', $yesterday);
          //$query->range(0,20);
          $output = $query->execute();
         // $output = $query->distinct()->execute();

          $i=0;
          while ($rows = $output->fetchAssoc()) {
              $data_loading[$i]['date'] = date('d-m-Y',strtotime($rows['date']));
              $data_loading[$i]['count'] = $rows['count'];
              $i++;
          }


          return [
              '#theme' => 'data_loading_control',
              '#data_loading' => $data_loading,
              '#cache' => ['max-age' => 0]
          ];
      }
}