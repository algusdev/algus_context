<?php

/**
 * @return
 * Contains \Drupal\algus_google_api\GoogleServiceSheets
 */
 
namespace Drupal\algus_google_api;

use Google_Client;
use Google_Service_Sheets;
use Drupal\Core\StreamWrapper\PublicStream;
 
/**
 * Provides route responses for the algus_google_api module.
 */
class GoogleServiceSheets {

  
   public function __construct() {

   }
  
  
   public function initGoogleServiceSheets($auth_key_file) {
       
        $google_account_auth_key_file_path = PublicStream::basePath().'/gkeys/'.$auth_key_file;
    
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$google_account_auth_key_file_path);
          
        // Документация https://developers.google.com/sheets/api/
        $googleClient = new Google_Client();
        
        // Устанавливаем полномочия
        $googleClient->useApplicationDefaultCredentials();
         
        // Области, к которым будет доступ
        $googleClient->addScope('https://www.googleapis.com/auth/spreadsheets');
         
        return(new Google_Service_Sheets($googleClient));

   }
}